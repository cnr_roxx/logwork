///
/// Handling datafile
///

use std::path::PathBuf;
use std::fs;
use std::fs::{File, OpenOptions, read_to_string, remove_file, rename};
use std::io;
use std::io::{Write};
use anyhow::{anyhow, Result};
use crate::prelude::*;

pub fn get_temp_datafile_path() -> Option<PathBuf> {
    match dirs::home_dir() {
        Some(homedir)=> {
            let mut temp_datafile_path = homedir;
            temp_datafile_path.push(crate::DATADIR);
            temp_datafile_path.push(crate::TEMP_DATAFILE);
            //println!("Expected temp datafile path: {:?}", temp_datafile_path);
            Some(temp_datafile_path)
        },
        None => {
            None
        }
    }
}

fn get_datadir_path() -> Option<PathBuf> {
    match dirs::home_dir() {
        Some(homedir)=> {
            let mut datadir_path = homedir;
            datadir_path.push(crate::DATADIR);
            //println!("Expected datadir path: {:?}", datadir_path);
            Some(datadir_path)
        },
        None => None
    }
}

// Let's concatenate expected path to logwork's data dir
// Return "none option" if it's can not be created
pub fn get_datafile_path() -> Option<PathBuf> {
    match dirs::home_dir() {
        Some(homedir)=> {
            let mut datafile_path = homedir;
            datafile_path.push(crate::DATADIR);
            datafile_path.push(crate::DATAFILE_NAME);
            //println!("Expected datafile path: {:?}", datafile_path);
            Some(datafile_path)
        },
        None => None
    }
}

pub fn is_homedir_available() -> bool {
    match dirs::home_dir() {
        Some(homedir_path)=> {
            match fs::metadata(homedir_path) {
                Ok(homedir_metadata) => {
                    //println!("Homedir: {:?}", homedir_metadata);
                    return true
                },
                Err(e) => {
                    println!("There is no user's homedir in the filesystem");
                    return false
                }
            }

        },
        None => {
            println!("There is no user's homedir defined in the filesystem");
            return false
        }
    }
}

pub fn is_datadir_available() -> bool {
    match get_datadir_path() {
        Some(datadir_path) => {
            match fs::metadata(datadir_path) {
                Ok(datadir_metadata) => {
                    //println!("Datadir: {:?}", datadir_metadata);
                    return true
                },
                Err(e) => {
                    println!("Datadir path does not exists. Error: {:?}", e);
                    return false
                }
            }
        },
        None => {
            println!("Can not localize datadir");
            return false
        }
    }

}

pub fn is_datafile_available() -> bool {
    match get_datafile_path() {
        Some(datafile_path) => {
            match fs::metadata(datafile_path) {
                Ok(datafile_path) => {
                    //println!("Datafile path '{:?}' exists", datafile_path);
                    return true
                },
                Err(e) => {
                    println!("Datafile path does not exists. Error: {:?}", e);
                    if is_datadir_available() {
                        println!("Will try to create datafile");
                    } else {
                        if is_homedir_available() {
                            println!("Will try to create datadir");
                        } else {
                            println!("Homedir is not available. Can not create datafile. Exitining");
                            return false
                        }
                    }
                    return false
                }
            }
        }
        None => {
            println!("Can not localize datadir");
            return false
        }
    }
}

fn create_datadir() -> anyhow::Result<()> {
    match get_datadir_path() {
        Some(datadir) => {
            fs::create_dir_all(datadir)?;
            Ok(())
        },
        None => {
            return Err(anyhow!("Can not generate datadir string"));
        }
    }
}

fn create_datafile(init_content: &str) -> anyhow::Result<()> {
    println!("Creating datafile...");

    match get_datafile_path() {
        Some(datafile_path) => {
            let display = datafile_path.display();

            // Open a file in write-only mode, returns `io::Result<File>`
            let mut file = match File::create(&datafile_path) {
                Err(why) => panic!("couldn't create {}: {}", display, why),
                Ok(file) => file,
            };

            // Write the `LOREM_IPSUM` string to `file`, returns `io::Result<()>`
            match file.write_all(init_content.as_bytes()) {
                Err(why) => panic!("couldn't write to {}: {}", display, why),
                Ok(_) => println!("successfully wrote to {}", display),
            }

            Ok(())
        },
        None => {
            return Err(anyhow!("Can not get datadir path"));
        }
    }

}

pub fn check_datafile(init_content: &str) -> bool {
    if !is_homedir_available() {
        return false
    }

    if !is_datadir_available() {
        create_datadir();
    }

    if !is_datafile_available() {
        create_datafile(init_content);
    }

    true
}

