#![allow(unused)]
use std::fs;
use std::fs::{File, OpenOptions, read_to_string, remove_file, rename};
use std::env;
use std::error::Error;
use std::io;
use std::io::{Write};
use std::path::{Path, PathBuf};
use std::process;
use std::ops::Sub;
use std::process::exit;
use clap::Parser;
use serde::Deserialize;
use chrono::prelude::*;
use anyhow::{anyhow, Result};
use tabled::{Tabled, Table, TableIteratorExt, Disable, Style, Modify, Alignment, object::Segment};

mod files_ops;
mod prelude {
    pub use crate::files_ops::*;
}
use prelude::*;

const TIME_FORMAT_PATTERN_FULL: &str = "%Y-%m-%d %H:%M:%S";
const TIME_FORMAT_PATTERN_HM: &str = "%H:%M";
const DATADIR: &str = ".logwork";
const DATAFILE_NAME: &str = "logwork_log.csv";
const TEMP_DATAFILE: &str = "logwork_log.tmp";
const CSV_COLUMN_HEADERS: &str = "TimeFrom,TimeTo,Duration,Task,TaskType,Project,Company\n";

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
/// A simple work logging tool
struct Cli {
    // The command to execute
    command: String,

    #[clap(short, long)]
    task: Option<String>,

    #[clap(short, long)]
    company: Option<String>,

    #[clap(long)]
    task_type: Option<String>,

    #[clap(short, long)]
    project: Option<String>,

    #[clap(long)]
    to: Option<String>,

    #[clap(short, long)]
    from: Option<String>,

}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
#[derive(Tabled)]
#[derive(Clone)]
struct Record {
    time_from: String,
    time_to: String,
    duration: String,
    task: String,
    task_type: String,
    project: String,
    company: String,
}
impl Record {
    pub fn new() -> Self {
        Self {
            time_from: String::new(),
            time_to: String::new(),
            duration: String::new(),
            task: String::new(),
            task_type: String::new(),
            project: String::new(),
            company: String::new(),
        }
    }
}

#[derive(Tabled)]
struct RecordDTO {
    time_from: String,
    time_to: String,
    duration: String,
    task: String,
    task_type: String,
    project: String,
    company: String,
}
impl RecordDTO {
    pub fn new() -> Self {
        Self {
            time_from: String::new(),
            time_to: String::new(),
            duration: String::new(),
            task: String::new(),
            task_type: String::new(),
            project: String::new(),
            company: String::new(),
        }
    }
}

///
/// Start command functions
///
fn read_csv_as_vector() -> Result<(Vec<Record>), Box<dyn Error>> {

    let datafile_path = match get_datafile_path() {
        Some(datafile_path) => datafile_path,
        None => panic!("Couldn't get datafile path"),
    };

    let mut csv_content: Vec<Record> = Vec::new();

    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(datafile_path)?;
    for result in rdr.deserialize() {
        let record: Record = result?;
        csv_content.push(record);
    }

    println!("read_csv_as_vector(): Records in file: {:?}", &csv_content.len());

    Ok(csv_content)
}



fn write_csv(records: Vec<Record>) -> Result<(), Box<dyn Error>> {

    let temp_datafile_path = match get_temp_datafile_path() {
        Some(temp_datafile_path) => temp_datafile_path,
        None => panic!("Couldn't get temp datafile path"),
    };

    let mut file_w = OpenOptions::new()
        .write(true)
        .create(true)
        .open(temp_datafile_path)
        .unwrap();

    let mut wtr = csv::WriterBuilder::new()
        .from_writer(file_w);

    println!("write_csv(): Records in file: {:?}", records.len());

    // Writing all the records and updating 'time_to' in the last record,
    // which also has an empty 'time_to' field.
    // From now it will have the 'now' value in 'time_to' field.
    for record in records.iter() {
        println!("Writing record: {:?}", record);
        wtr.write_record([&record.time_from, &record.time_to, &record.duration, &record.task,
                    &record.task_type, &record.project, &record.company]);
    }

    // A CSV writer maintains an internal buffer, so it's important
    // to flush the buffer when you're done.
    wtr.flush()?;


    // Handling orginal and temp files
    let datafile_path = match get_datafile_path() {
        Some(datafile_path) => datafile_path,
        None => panic!("Couldn't get datafile path"),
    };
    let datafile_temp_path = match get_temp_datafile_path() {
        Some(datafile_path) => datafile_path,
        None => panic!("Couldn't get datafile path"),
    };

    remove_file(&datafile_path);
    rename(&datafile_temp_path, &datafile_path);

    Ok(())
}

///
/// Stop command
///

fn stop(time_to: Option<NaiveDateTime>) {
    match read_csv_as_vector() {
        Ok(mut records) => {

            if (records.len() > 1) {

                match records.last_mut() {
                    Some(mut last_record) => {
                        if (last_record.time_to == "") {
                            match time_to {
                                Some(tt) => {
                                    println!("Time to: {}", tt);
                                    last_record.time_to = tt.format(TIME_FORMAT_PATTERN_FULL).to_string();
                                },
                                None => {
                                    println!("There is no timeTo");
                                    let now_time = Local::now();
                                    last_record.time_to = now_time.format(TIME_FORMAT_PATTERN_FULL).to_string();
                                },
                            }

                            // Calculate the duration of last task
                            let duration_seconds = calculate_duration_in_seconds(&last_record.time_from, &last_record.time_to);
                            last_record.duration = duration_seconds.to_string();

                            write_csv(records);

                        } else {
                            println!("Last task is already stopped")
                        }
                    },
                    None => println!("No tasks to stop")
                }

            } else {
                println!("No tasks to stop")
            }

        },
        Err(e) => println!("Error while reading CSV file as vector: {}", e),
    }
}


///
/// Start command
///

fn start(time_from: Option<NaiveDateTime>, task: Option<String>, task_type: Option<String>, project: Option<String>,
         company: Option<String>) {

    println!("Running 'start' function");

    let mut task_str = String::new();
    let mut time_from_str = String::new();

    match read_csv_as_vector() {
        Ok(mut records) => {

            let now_time = Local::now();
            let now: String = now_time.format(TIME_FORMAT_PATTERN_FULL).to_string();

            match time_from {
                Some(tf) => {
                    // In case user passed some value as 'time-from' we will set it as
                    // * 'last_record.time_to' (when the current value of last_record.time_to' has some value different than empty string
                    // * 'current_record.time_from'
                    time_from_str = tf.format(TIME_FORMAT_PATTERN_FULL).to_string();
                },
                None => {
                    // In case user doesn't pass any value as 'time-from' we will set 'now' timestamp in places mentioned in previous comment
                    time_from_str = now;
                }
            }

            // When user doesn't provide parameters, 'start' command copies their values from last log entry
            // But when there is no entries yet (only CSV header) then non-existent parameters are stored as empty strings
            let empty_record = Record::new();
            let mut last_record = &empty_record;

            if (records.len() > 1) {

                match records.last_mut() {
                    Some(mut lr) => {
                        if (lr.time_to.eq("")) {
                            println!("Stopping last task");
                            // TODO: Try to use &String or &str instead of String as Struct fields timeTo and TmeFrom
                            lr.time_to = time_from_str.clone();

                            // Calculation the duration of last task
                            let duration_seconds = calculate_duration_in_seconds(&lr.time_from, &time_from_str);
                            if duration_seconds <= 0 {
                                println!("Current task has to last at least 1s. Update current task first or start new task later than {:?}.", &lr.time_from);
                                process::exit(1);
                            }
                            lr.duration = duration_seconds.to_string();

                        } else {
                            println!("Checking if new task doesn't begins before last task and recalculating duration of last task");
                            // Here we have to check if we can start new task, i.e. the time_from of
                            // the new task is later than time_from of the last task in the log
                            last_but_one_time_update(lr, &time_from_str);
                        }
                    }
                    None => println!("No previous task found")
                }

                last_record = records.last().unwrap();
            }

            let mut new_record: Record = Record::new();

            // Set current_record.time_form to time_from_str
            new_record.time_from = time_from_str.clone();

            match task {
                Some(t) => {
                    println!("Task: {}", t);
                    new_record.task = t;
                },
                None => {
                    println!("There is no task");
                    new_record.task = last_record.task.to_string();
                },
            }

            match task_type {
                Some(tt) => {
                    println!("Task_type: {}", tt);
                    new_record.task_type = tt;
                },
                None => {
                    println!("There is no task_type");
                    new_record.task_type = last_record.task_type.to_string();
                },
            }

            match project {
                Some(eb) => {
                    println!("Project: {}", eb);
                    new_record.project = eb;
                },
                None => {
                    println!("There is no project");
                    new_record.project = last_record.project.to_string();
                },
            }

            match company {
                Some(c) => {
                    println!("Company: {}", c);
                    new_record.company = c;
                },
                None => {
                    println!("There is no company");
                    new_record.company = last_record.company.to_string();
                },
            }

            println!("Records in: {:?}", records.len());

            // Adding current_record to the list
            records.push(new_record.clone());

            // Writing new file content
            write_csv(records);

            // Display new record
            let mut new_record_dto = RecordDTO::new();
            new_record_dto.time_from = new_record.time_from;
            new_record_dto.time_to = new_record.time_to;
            new_record_dto.company = new_record.company;
            new_record_dto.project = new_record.project;
            new_record_dto.task = new_record.task;
            new_record_dto.task_type = new_record.task_type;
            new_record_dto.duration = new_record.duration;
            let mut for_display: Vec<RecordDTO> = Vec::new();
            for_display.push(new_record_dto);
            
            display_table(&for_display);

        },
        Err(e) => println!("Error while reading CSV file as vector: {}", e),


    }
}

///
/// Update command
///
fn update(time_from: Option<NaiveDateTime>, time_to: Option<NaiveDateTime>, task: Option<String>,
          task_type: Option<String>, project: Option<String>, company: Option<String>) {
    println!("Running 'update' function");
    match read_csv_as_vector() {
        Ok(mut records) => {
            if (records.len() > 1) {

                let mut time_from_calc = String::new();

                match records.last_mut() {
                    Some(mut lr) => {

                        // First check if user set "--time-from" param
                        // If not - get for duration calculation "time_from" from current record
                        time_from_calc = match time_from {
                            Some(tf) => tf.format(TIME_FORMAT_PATTERN_FULL).to_string(),
                            None => lr.time_from.clone()
                        };

                        // Then check if user set "--time-to" param
                        // If not - get for duration calculation "time_to" from current record
                        let time_to_calc = match time_to {
                            Some(tto) => tto.format(TIME_FORMAT_PATTERN_FULL).to_string(),
                            None => lr.time_to.clone()
                        };

                        // If "time_to_calc" is not empty we're gonna calculate duration
                        // If duration is not negative we're updating lr.time_to and duration fields
                        // If duration is negative we're exiting without writing any changes
                        if (!time_to_calc.eq("")) {
                            //
                            let duration_seconds = calculate_duration_in_seconds(&time_from_calc, &time_to_calc);
                            if duration_seconds <= 0 {
                                println!("It's impossible for human to do something meaningful in {:?}s. Task has to last at least 1s.", &duration_seconds);
                                process::exit(1);
                            } else {
                                lr.time_to = time_to_calc;
                                lr.duration = duration_seconds.to_string();
                            }
                        }

                        lr.time_from = time_from_calc.clone();


                        match task {
                            Some(t) => {
                                println!("Updating task: {}", t);
                                lr.task = t;
                            },
                            None => {
                                println!("There is no task to update");
                            },
                        }

                        match task_type {
                            Some(tt) => {
                                println!("Updating task_type: {}", tt);
                                lr.task_type = tt;
                            },
                            None => {
                                println!("There is no task_type to update");
                            },
                        }

                        match project {
                            Some(eb) => {
                                println!("Updating project: {}", eb);
                                lr.project = eb;
                            },
                            None => {
                                println!("There is no project to update");
                            },
                        }

                        match company {
                            Some(c) => {
                                println!("Updating company: {}", c);
                                lr.company = c;
                            },
                            None => {
                                println!("There is no company to update");
                            },
                        }

                    },
                    None => {
                        println!("Can not fetch last record for update");
                    }
                };


                // Let's check if we're also need to update 'last-but-one' record
                let last_but_one_index = records.len().wrapping_sub(2);
                if let Some(last_but_one) = records.get_mut(last_but_one_index) {
                    last_but_one_time_update(last_but_one, &time_from_calc);
                }

                // Let's ask user if her knows what she's doing
                println!("Do you really want to UPDATE last record? (y/n)");
                let decision:String = read_line();
                if decision.eq("y") {
                    println!("Ok, I'm updating data in log-file");
                    write_csv(records);
                } else {
                    println!("Ok, I'm cancelling changes");
                }



            } else {
                println!("Nothing to update")
            }
        },
        Err(e) => println!("Error while reading CSV file as vector: {}", e),
    }
}

fn last_but_one_time_update(last_but_one:&mut Record, time_from_calc :&String) {
    let duration_between_tasks = calculate_duration_in_seconds(&last_but_one.time_to, &time_from_calc);
    if duration_between_tasks < 0 {
        println!("Last task is going to begin before the one-before-last ends.");
        let duration_between_time_from_of_two_tasks = calculate_duration_in_seconds(&last_but_one.time_from, &time_from_calc);
        if duration_between_time_from_of_two_tasks <= 0 {
            println!("Last task is going to begin before or at the same moment as the one-before-last begins. Sorry, we can not do such change");
            // Make sure that in case of changes to one-before-last are not possible then changes to last record should be cancelled as well
            process::exit(1);
        } else {
            println!("Last task is going to begin after the one-before-last begins. We need to change 'one-before-last.time_to' field and recalculate its duration");
            last_but_one.time_to = time_from_calc.clone();
            let new_duration = calculate_duration_in_seconds(&last_but_one.time_from, &last_but_one.time_to);
            last_but_one.duration = new_duration.to_string();

            println!("One before last after changes: {:?}", last_but_one);
        }
    } else {
        println!("Last task is going to begin after or in the same moment as the one-before-last ends. In that case we do not have to modify one-before-last task");
    }
}


///
/// List command
///
fn format_seconds_duration(seconds: i64) -> String {

    let sec = seconds % 60;
    let minutes = (seconds / 60) % 60;
    let hours = (seconds / 60) / 60;

    format!("{:0>2}:{:0>2}:{:0>2}", hours, minutes, sec)
}

fn list(time_from: Option<String>, time_to: Option<String>, task: Option<String>,
        task_type: Option<String>, project: Option<String>, company: Option<String>) {

    let mut list_params :Vec<(&str, String)> = Vec::new();

    match time_from {
        Some(tfa) => {
            list_params.push(("time_from", String::from(&tfa)));
        },
        None => {}
    };

    match time_to {
        Some(tt) => {
            list_params.push(("time_to", String::from(&tt)));
        },
        None => {}
    };

    match task {
        Some(t) => {
            list_params.push(("task", String::from(&t)));
        },
        None => {}
    };

    match task_type {
        Some(tty) => {
            list_params.push(("task_type", String::from(&tty)));
        },
        None => {}
    };

    match project {
        Some(p) => {
            list_params.push(("project", String::from(&p)));
        },
        None => {}
    };

    match company {
        Some(c) => {
            list_params.push(("company", String::from(&c)));
        },
        None => {}
    };

    let datafile_path = match get_datafile_path() {
        Some(datafile_path) => datafile_path,
        None => panic!("Couldn't get datafile path"),
    };

    let mut rdr = match csv::ReaderBuilder::new()
        .has_headers(true)
        .from_path(datafile_path) {
        Ok(rdr) => rdr,
        Err(e) => {
            process::exit(1);
        }
    };

    let mut records_dto: Vec<RecordDTO> = Vec::new();
    let mut total_duration = 0;
    let mut unfinished_duration = 0;

    for result in rdr.deserialize() {
        match result {
            Ok(res) => {
                let record: Record = res;

                let mut display_record = true;
                for param in &list_params {

                    match param.0 {
                        "time_from" => {
                            if !record.time_from.contains(&param.1) {
                                display_record = false;
                            }
                        }
                        "time_to" => {
                            if !record.time_to.contains(&param.1) {
                                display_record = false;
                            }
                        }
                        "task" => {
                            if !record.task.contains(&param.1) {
                                display_record = false;
                            }
                        }
                        "task_type" => {
                            if !record.task_type.contains(&param.1) {
                                display_record = false;
                            }
                        }
                        "project" => {
                            if !record.project.contains(&param.1) {
                                display_record = false;
                            }
                        }
                        "company" => {
                            if !record.company.contains(&param.1) {
                                display_record = false;
                            }
                        }
                        _ => {}
                    }
                }

                if display_record {

                    let record_duration :i64 = match record.duration.parse() {
                        Ok(seconds) => seconds,
                        Err(e) => 0
                    };

                    total_duration = total_duration + record_duration;

                    if (record_duration == 0) {
                        let now_time = Local::now();
                        let now: String = now_time.format(TIME_FORMAT_PATTERN_FULL).to_string();
                        unfinished_duration = calculate_duration_in_seconds(&record.time_from, &now);
                    }

                    let mut record_dto: RecordDTO = RecordDTO::new();
                    record_dto.time_from = record.time_from;
                    record_dto.time_to = record.time_to;
                    record_dto.duration = format_seconds_duration(record_duration);
                    record_dto.task = record.task;
                    record_dto.task_type = record.task_type;
                    record_dto.project = record.project;
                    record_dto.company = record.company;
                    records_dto.push(record_dto);
                }

            },
            Err(e) => println!("Error while deserializing record: {:?}", e)
        }

    }

    display_table(&records_dto);
    println!("Displayed records: {}, duration: {} (with unfinished: {})",
             &records_dto.len(), format_seconds_duration(total_duration),
             format_seconds_duration(total_duration + unfinished_duration));

}

fn display_table(records_dto: &Vec<RecordDTO>) {
    let table = Table::new(records_dto)
        // .with(Disable::Row(..1))
        .with(Style::modern())
        .with(Modify::new(Segment::all()).with(Alignment::left()))
        .to_string();
    print!("{}", table);
}

///
/// Choice
///
fn read_line() -> String {
    let mut line = String::new();
    std::io::stdin().read_line(&mut line).unwrap();
    return line.trim().to_string();
}

///
/// Duration handling
///

fn calculate_time_in_seconds(time_from: NaiveDateTime, time_to: NaiveDateTime) -> i64 {
    let diff = time_to - time_from;
    println!("Duration of task (from: {:?} to {:?}): {:?}", time_from, time_to, diff.num_seconds());
    diff.num_seconds()
}

fn calculate_duration_in_seconds(time_from: &str, time_to: &str) -> i64 {
    // Calculation the duration of last task
    let time_from_naive_dt = NaiveDateTime::parse_from_str(time_from, TIME_FORMAT_PATTERN_FULL);
    let time_to_naive_dt = NaiveDateTime::parse_from_str(time_to, TIME_FORMAT_PATTERN_FULL);
    calculate_time_in_seconds(time_from_naive_dt.unwrap(), time_to_naive_dt.unwrap())
}

fn generate_naive_date_time(time_str: &str) -> Option<NaiveDateTime> {
    if let Ok(time) = NaiveDateTime::parse_from_str(time_str, TIME_FORMAT_PATTERN_FULL) {
        Some(time)
    } else {
        if let Ok(time) = NaiveTime::parse_from_str(time_str, TIME_FORMAT_PATTERN_HM) {
            Some(Local::today().and_hms(time.hour(), time.minute(), 00).naive_local())
        } else {
            None
        }
    }
}

fn generate_date_time(datetime: &Option<String>) -> Option<NaiveDateTime> {
    match &datetime {
        Some(from) => {
            println!("--from arg: {:?}", &from);
            if let Some(calculated_time) = generate_naive_date_time(&from) {
                println!("Calculated time: {:?}", calculated_time.format(TIME_FORMAT_PATTERN_FULL).to_string());
                Some(calculated_time)
            } else {
                println!("--from argument passed but unparsable, quitting. ");
                process::exit(1);
            }

        }
        None => None
    }
}

///
/// Main function
///

fn main() {

    match check_datafile(CSV_COLUMN_HEADERS) {
        true => println!("Datafile is available"),
        false => {
            println!("Datafile is not available and can not be created");
            process::exit(1);
        }
    }

    let args = Cli::parse();

    println!("Executing command: {}", args.command);


    match args.command.as_str() {
        "stop" => {
            let time_to = generate_date_time(&args.to);
            stop(time_to);
        },
        "start" => {
            let time_from = generate_date_time(&args.from);
            start(time_from, args.task, args.task_type, args.project, args.company)
        },
        "list" => list(args.from, args.to, args.task, args.task_type,
                       args.project, args.company),
        "update" => {
            let time_to = generate_date_time(&args.to);
            let time_from = generate_date_time(&args.from);
            update(time_from, time_to, args.task, args.task_type,
                   args.project, args.company)
        },
        _ => println!("No such command")
    }

}