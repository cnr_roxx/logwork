# logwork

Simple tool for logging work and my way to learn some Rust

## Description
The concept is to quickly implement simple but useful tool for logging work. You know just throw some shell commands instead of filling up spreadsheet.

It is also meant as a tool for learning Rust and improve skills in this language

## Building

### Building

```shell
crate build --release
```

## Usage

### Glossary
* Current task - last entry in the log with empty "time_to" field. Basically represents task on which You're working right now.
* Last task - last entry in the log.

### Start logging the task

Start logging the task since now.
```shell
logwork start --task "Counting stars" --task-type "Astrophysics" --project "BlueDot" --company "NASA"
```

Start logging the task since specific point in time:
```shell
logwork start --task "Counting stars" --task-type "Astrophysics" --project "BlueDot" --company "NASA" --time-from "2022-05-18 21:02:05"
```

Start logging new task always means stop logging the current task with exact the same time. 

So if you start logging new task with '2022-05-18 21:19:34' timestamp, the current task will be stopped on '2022-05-18 21:19:34' (if it's not stopped already). 

### Update last task
Update fields in last task in log

```shell
logwork update --task "Counting stars" --task-type "Astrophysics" --project "BlueDot" --company "NASA" --time-from "2022-05-23 21:40:56" --time-to "2022-05-23 21:45:19"
```

### Stop logging
 
```shell
logwork stop
```
Sets current time as "time_to" in current task.

```shell
logwork stop --time-to "2022-05-17 16:15:17"
```
Sets arbitrary time as "time_to" in current task.

### List tasks

```shell
logwork list
```
Lists all entries stored in datafile

```shell
logwork list --time-from "05-28"
```
Lists all entries containing phrase "05-28" in time_from field

## Roadmap

* Check date format passed in 'time-from' and 'time-to' parameters (regex?)
* All commands which are modifying timestamp fields should accept arguments only in two formats:
  * as "full" timestamp ("YYYY-mm-dd HH:mm")
  * as hour-minute ("HH:mm")
* Add integration with Jira
  * Pushing worklogs to Jira
  * Pulling task names from Jira
* Add 'delete' command: deletes last entry in log
* Add 'reopen' command: clears 'time_to' field in last entry in log



## Contributing

At current state of things sending the comment is the only way of contributing.

## License
https://gitlab.com/cnr_roxx/logwork/-/blob/master/LICENSE
